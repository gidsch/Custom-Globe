// ==UserScript==
// @name         Custom Globe
// @namespace    
// @version      1.2.5
// @description  Custom Globe, No header Fitlerd News right sidebar moved
// @author       Kilian, Schroll
// @match        https://globe.alzchemnet.de/*

// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    $("body").css("overflow-x","hidden");
    $("#siteIcon").hide();
    $("#notfallmanagement").hide();
    var blockedNews=["Sonderangebot in der Café-Bar",
    "Anpassungen der Organisationsstrukturen",
    "Personelle Veränderungen",
    "Neue PSA freigegeben",
    "Faschingskrapfen",
    "Assistentinnen-Weiterbildung",
    "Seminar",
    "gesund punkten",
    "Sonderangebot in der Kaffeebar",
    "Sonderangebot in der Cafebar",
    "Sonderangebot",
    "SAP",
    "für Kontraktoren",
    "Tagescheckliste",
    "Wahlausschreiben",
    "Verbesserungsvorschl",
    "Aramark",
    "Sicherheit",
    "Gutscheinbücher",
    "Degussa Bank",
    "Einladung",
    "PC-Schulung",
    "Kleinchemikaliensammlung",
    "Nichtraucherseminar",
    "Interne Sprachkurse",
    "Neu erstellte Verfahrensanweisung",
    "Cafébar",
    "Café-Bar",
    "Aktualisierte Unternehmensleitlinien",
    "Betriebsratswahl",
    "KSUND",
    "Jahre unfallfrei im Bereich",
    "Jahre unfallfrei in",
    "Sicherheitsleistung",
    "Mein Beitrag zur Sicherheitsleistung",
    "Jahren ohne meldepflichtigen Arbeitsunfall",
    "Geänderte Verfahrensanweisung",
    "Vorteile für Mitarbeiter",
    "Schulung",
    "PSA",
    "Neues Seminar",
    "Straßensperrung",
    "Pressemitteilung",
    "Personelle Veränderungen Nr",
    "Weihnachtssingen",
    "Weihnachtsgruß",
    "IMS-Programm",
    "IMS",
    "USGQ",
    "Webinar"];
     $(".newslist .news .heading").each(function(){
        if (new RegExp(blockedNews.join("|")).test($(this)[0].textContent)) {
            console.log("Blocked:" + $(this)[0].textContent);
            $(this).parent().parent().hide();
        }
    });
    $(".newslist").before("<td><h1><a id='removeFilter' href='#'>#Filterd#</a></h1></td>");
    //Move Speiseplan
    var e = $("#SPContainerheader");
    e.prev().insertAfter(e);
    e.prev().insertAfter(e);
    //Move Meine Links
    var f = $("#terminecontainer").next();
    f.prev().insertAfter(f);

    //Fehlerteufel entfernen
    $(".rightcolumn.fehlerteufel.frame").remove();
})();
$("#removeFilter").on("click", function(){
$(".newslist .news .heading").each(function(){
            $(this).parent().parent().show();
    });
    //Show Settings on Top Bar
    $(".ms-core-deltaSuiteLinks").find("li").remove();
    CoreInvoke('MMU_Open',byid('zz5_SiteActionsMenuMain'), MMU_GetMenuFromClientId('zz11_SiteActionsMenu'));
    $(".ms-core-menu-box.ms-core-defaultFont.ms-shadow").find("li").each(function(){
        var s = '<li class="ms-core-suiteLink" onclick="' + $(this).attr("onmenuclick") + '"><a class="ms-core-suiteLink-a" href="https://mysites.alzchemnet.de:443/default.aspx" id="ctl00_ctl59_ShellNewsfeed"><span>' + $(this).text() + '</span></a></li>';
    	$(".ms-core-suiteLinkList").append(s);
    });
    $(".ms-core-menu-box.ms-core-defaultFont.ms-shadow").css("visibility","hidden");
    $(".ms-core-deltaSuiteLinks").show();
});
